# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)

find_package(PkgConfig REQUIRED)
pkg_check_modules(CPPUNIT cppunit)

find_package_handle_standard_args(CPPUNIT
    REQUIRED_VARS CPPUNIT_LIBRARIES)

add_library(cppunit UNKNOWN IMPORTED)

if(CPPUNIT_FOUND)
    set_target_properties(cppunit PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${CPPUNIT_INCLUDE_DIRS}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
        IMPORTED_LOCATION "${CPPUNIT_LINK_LIBRARIES}")
endif()
