# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(CheckFunctionExists)

option(TESTS "Compile and run unit tests" OFF)

check_function_exists(getopt_long HAVE_GETOPT_LONG)

# CppUnit now requires a recent version of C++
# turn this always on
set(CMAKE_CXX_STANDARD 11)

if(TESTS)
    find_package(CPPUNIT REQUIRED)

    add_custom_target(test-verbose
        COMMAND ${CMAKE_CTEST_COMMAND} --verbose
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")

    enable_testing()
endif()