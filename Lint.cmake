# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# Generate compile_commands.json file (for cquery or other linting tools)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
add_custom_target(compile_commands_copy ALL
        COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_BINARY_DIR}/compile_commands.json"
            "${CMAKE_SOURCE_DIR}/compile_commands.json"
        DEPENDS "${CMAKE_BINARY_DIR}/compile_commands.json")

function(_lint_list_compact OUT)
  set(ret)
  foreach(v IN LISTS "${OUT}")
    if(v)
      list(APPEND ret "${v}")
    endif()
  endforeach()
  set("${OUT}" "${ret}" PARENT_SCOPE)
endfunction()

function(_lint_target target OUT)
  get_target_property(inc "${target}" INCLUDE_DIRECTORIES)
  get_target_property(inci "${target}" INTERFACE_INCLUDE_DIRECTORIES)
  get_target_property(incisys "${target}" INTERFACE_SYSTEM_INCLUDE_DIRECTORIES)
  set(incs ${inc} ${inci} ${incisys})
  _lint_list_compact(incs ret)
  if(incs)
    list(APPEND "${OUT}" ${incs})
    list(REMOVE_DUPLICATES "${OUT}")
  endif()

  get_target_property(linklibs "${target}" LINK_LIBRARIES)
  foreach(link IN LISTS linklibs)
    if(TARGET "${link}")
      _lint_target("${link}" "${OUT}")
    endif()
  endforeach()
  set("${OUT}" "${${OUT}}" PARENT_SCOPE)
endfunction()

function(_lint_includes dir OUT)
  get_property(targets DIRECTORY "${dir}" PROPERTY BUILDSYSTEM_TARGETS)
  get_property(subd DIRECTORY "${dir}" PROPERTY SUBDIRECTORIES)
  foreach(target IN LISTS targets)
    _lint_target("${target}" "${OUT}")
  endforeach()
  foreach(sub IN LISTS subd)
    _lint_includes("${sub}" "${OUT}")
  endforeach()
  set("${OUT}" "${${OUT}}" PARENT_SCOPE)
endfunction()
_lint_includes("${CMAKE_SOURCE_DIR}" INCDIRS)
message(VERBOSE "Linter detected include dirs: ${INCDIRS}")

list(APPEND SRCDIRS src tests)

include(LintCQuery)

find_program(CLANG_FORMAT_EXE NAMES cquery-clang-format
    HINTS /opt/cquery/bin)
find_program(CLANG_FORMAT_EXE NAMES clang-format)
if(CLANG_FORMAT_EXE)
    message(STATUS "Clang-format found")
    add_custom_target(style
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}"
            -P "${CMAKE_CURRENT_LIST_DIR}/ClangFormatScript.cmake"
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")
else(CLANG_FORMAT_EXE)
    message(STATUS "Clang-format not found")
endif(CLANG_FORMAT_EXE)

# Clang configuration
find_program(CLANGPP_EXE NAMES clang++
    PATHS ${CLANGPP_EXE} ENV PATH $ENV{CLANGPP_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)
if(CLANGPP_EXE)
    message(STATUS "Clang++ linter found")
    set(CLANG_COMPLETE "${CMAKE_SOURCE_DIR}/.clang_complete")

    file(WRITE "${CLANG_COMPLETE}" "")
    file(APPEND "${CLANG_COMPLETE}" "--std=c++11\n")
    file(APPEND "${CLANG_COMPLETE}" "-I./src\n")
    file(APPEND "${CLANG_COMPLETE}" "-I${CMAKE_BINARY_DIR}/src\n")
    file(APPEND "${CLANG_COMPLETE}" "-I${SYSROOT}/usr/include\n")
    file(APPEND "${CLANG_COMPLETE}" "-I${SYSROOT}/usr/local/include\n")

    foreach(DIR ${INCDIRS})
        file(APPEND "${CMAKE_SOURCE_DIR}/.clang_complete" "-I${DIR}\n")
    endforeach(DIR)
    add_custom_target(lint-clang
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintClangScript.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    add_custom_target(lint-clang-clean
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}" -DMODE="clean"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintClangScript.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    list(APPEND LINT_TGT lint-clang)
    list(APPEND LINT_TGT_CLEAN lint-clang-clean)
else(CLANGPP_EXE)
    message(STATUS "Clang++ linter not found")
endif(CLANGPP_EXE)

find_program(CPPCHECK_EXE NAMES cppcheck
    PATHS ${CPPCHECK_EXE} ENV PATH $ENV{CLANGPP_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)
if(CPPCHECK_EXE)
    message(STATUS "CppCheck linter found")
    add_custom_target(lint-cppcheck
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintCppCheckScript.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    add_custom_target(lint-cppcheck-clean
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}" -DMODE="clean"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintCppCheckScript.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    list(APPEND LINT_TGT lint-cppcheck)
    list(APPEND LINT_TGT_CLEAN lint-cppcheck-clean)
else(CPPCHECK_EXE)
    message(STATUS "CppCheck linter not found")
endif(CPPCHECK_EXE)

find_program(GCC_EXE NAMES gcc g++
    PATHS ${GCC_EXE} ENV PATH $ENV{GCC_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)
if(GCC_EXE)
    execute_process(
        COMMAND "${GCC_EXE}" --version
        OUTPUT_VARIABLE GCC_VERSION
        ERROR_QUIET)
    if(GCC_VERSION MATCHES " LLVM ")
        message(STATUS "LLVM-gcc not suitable for linting")
        # Looking harder
        unset(GCC_EXE CACHE)
        find_program(GCC_EXE NAMES gcc-7 g++-7 gcc-8 g++-8 gcc-9 g++-9
            PATHS ${GCC_EXE} ENV PATH $ENV{GCC_PATH}
            CMAKE_FIND_ROOT_PATH_BOTH)
    endif()
endif()
if(GCC_EXE)
    message(STATUS "GCC linter found")
    add_custom_target(lint-gcc
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintGCCScript.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    add_custom_target(lint-gcc-clean
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${SRCDIRS}" -DMODE="clean"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintGCCScript.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    list(APPEND LINT_TGT lint-gcc)
    list(APPEND LINT_TGT_CLEAN lint-gcc-clean)
else(GCC_EXE)
    message(STATUS "GCC linter not found")
endif()

if(LINT_TGT)
    add_custom_target(lint DEPENDS ${LINT_TGT})
    add_custom_target(lint-clean DEPENDS ${LINT_TGT_CLEAN})
endif(LINT_TGT)

add_custom_target(lint-badge
    COMMAND ${CMAKE_COMMAND}
        -DCMAKE_BUILD_TOOL="${CMAKE_BUILD_TOOL}"
        -P "${CMAKE_CURRENT_LIST_DIR}/LintBadge.cmake"
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")

