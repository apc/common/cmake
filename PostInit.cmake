# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

message(STATUS "CMake scripts loaded")

function(update_file destination source)
    file(MD5 "${source}" SRC_MD5)
    if(EXISTS "${destination}")
        file(MD5 "${destination}" DEST_MD5)
    endif()
    string(COMPARE EQUAL "${SRC_MD5}" "${DEST_MD5}" HASH_CMP)
    if(NOT HASH_CMP)
        get_filename_component(FILENAME "${destination}" NAME)
        message(STATUS "Updating ${FILENAME} file...")
        configure_file("${source}" "${destination}" COPYONLY)
    endif()
endfunction()

update_file("${CMAKE_SOURCE_DIR}/Init.cmake" "${CMAKE_CURRENT_LIST_DIR}/Init.cmake")
