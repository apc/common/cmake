# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(xProject)
include(Tools)

set(EDGE_VERSION 3.0.0)

add_library(edge UNKNOWN IMPORTED)
if(EXTERNAL_DEPENDENCIES OR EXTERNAL_EDGE)
    message(STATUS "Using external edge...")
    xProject_cache_load(VARIABLES EDGE_INCLUDE_DIRS EDGE_LIBRARIES EDGE_EXECUTABLE)
    set_default(CPU "L867")
    find_path(EDGE_INCLUDE_DIRS "edge/edge.h" PATHS
        "/acc/local/${CPU}/drv/edge/${EDGE_VERSION}/include")
    find_library(EDGE_LIBRARIES edge PATHS
        "/acc/local/${CPU}/drv/edge/${EDGE_VERSION}/lib")
    find_program(EDGE_EXECUTABLE edge PATHS
        "/usr/local/edge"
        "/acc/local/share/edge/")
endif()

xProject_Add(EDGE
    GIT_REPOSITORY "https://:@gitlab.cern.ch:8443/cohtdrivers/encore.git"
    GIT_TAG "edge-master"
    BUILD_COMMAND $(MAKE) CFLAGS+=-w
    UPDATE_COMMAND :
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/lib/libedge.so"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/include")

    add_dependencies(edge EDGE)

if(NOT EXTERNAL_EDGE)
    set(EDGE_EXECUTABLE "${CMAKE_BINARY_DIR}/instroot/edge/edge" CACHE PATH "edge executable")
    file(MAKE_DIRECTORY "${EDGE_INCLUDE_DIRS}")
endif()

find_package_handle_standard_args(EDGE
    REQUIRED_VARS EDGE_LIBRARIES EDGE_INCLUDE_DIRS EDGE_EXECUTABLE)

xproject_install(EDGE)

# FIXME remove when edge is properly linked with those
set(EDGE_LIBRARY ${EDGE_LIBRARIES})
list(APPEND EDGE_LIBRARIES rt pthread)

set_target_properties(edge PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${EDGE_INCLUDE_DIRS}"
    # FIXME remove when edge is properly linked with those
    INTERFACE_LINK_LIBRARIES "rt;pthread"
    IMPORTED_LINK_INTERFACE_LANGUAGES "C"
    IMPORTED_LOCATION "${EDGE_LIBRARY}")
