# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(CMakeParseArguments)

macro(use_cxx version)
  if(CMAKE_VERSION VERSION_LESS "3.1")
    if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++${version}")
    endif()
  else()
    set (CMAKE_CXX_STANDARD ${version})
  endif()
endmacro(use_cxx)

function(assert)
    cmake_parse_arguments(P "" "MESSAGE" "" ${ARGN})
    if(${P_UNPARSED_ARGUMENTS})
        # Can't use NOT since there may be several tests, so doing it that way
    elseif(P_MESSAGE)
        message(SEND_ERROR "${P_MESSAGE}")
    else()
        set(P_MESSAGE "Assertion failed:")
        foreach(PART ${P_UNPARSED_ARGUMENTS})
            set(P_MESSAGE "${P_MESSAGE} ${PART}")
        endforeach()
        message(SEND_ERROR "${P_MESSAGE}")
    endif()
endfunction()

function(set_default)
  cmake_parse_arguments(P "ENV" "" "" ${ARGN})
  list(GET P_UNPARSED_ARGUMENTS 0 variable)
  list(GET P_UNPARSED_ARGUMENTS 1 value)

  if (P_ENV AND DEFINED ENV{${variable}})
    set(${variable} $ENV{${variable}} PARENT_SCOPE)
  elseif(NOT DEFINED ${variable})
    set(${variable} ${value} PARENT_SCOPE)
  endif()
endfunction()