/*
** Copyright (C) 2022 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2022-07-13T10:12:45
** Author: Sylvain Fargier <sylvain.fargier@cern.ch>
*/

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static inline char digit(uint8_t c)
{
    return c + '0';
}

int main(int argc, char **argv)
{
    FILE *f_in, *f_out;
    const size_t buffer_size = 1024;
    char *buffer;
    size_t count, i, total;

    if (argc != 4)
    {
        fputs("Usage: mkbin {infile} {outfile} {varname}\n", stderr);
        return -1;
    }

    f_in = fopen(argv[1], "r");
    if (f_in == NULL)
    {
        fprintf(stderr, "Failed to open %s: %s", argv[1], strerror(errno));
        return -1;
    }

    f_out = fopen(argv[2], "w");
    if (f_out == NULL)
    {
        fprintf(stderr, "Failed to open %s: %s", argv[2], strerror(errno));
        fclose(f_in);
        return -1;
    }

    buffer = (char *) malloc(buffer_size);
    fputs("/* generated code, do not modify */\n", f_out);
    fputs("#include <string>\n", f_out);
    fprintf(f_out, "static const std::string %s{\\\n  \"", argv[3]);
    total = 0;
    while ((count = fread(buffer, 1, buffer_size, f_in)) > 0)
    {
        total += count;
        for (i = 0; i < count; ++i)
        {
            uint8_t c = buffer[i];
            if (c == '\n')
                fwrite("\\n\" \\\n  \"", 1, 9, f_out);
            else if (c == '\\' || c == '"')
            {
                fputc('\\', f_out);
                fputc(c, f_out);
            }
            else if (!isprint(c)) /* not ascii */
            {
                fputc('\\', f_out);
                fputc(digit((c >> 6) & 0x07), f_out);
                fputc(digit((c >> 3) & 0x07), f_out);
                fputc(digit(c & 0x07), f_out);
            }
            else
                fputc(c, f_out);
        }
    }

    if (ferror(f_in))
    {
        fprintf(stderr, "Read error on %s: %s", argv[1], strerror(errno));
        fclose(f_out);
        fclose(f_in);
        return -1;
    }
    fprintf(f_out, "\", %lu};\n", total);
    fclose(f_in);
    fclose(f_out);
    free(buffer);
    return 0;
}
